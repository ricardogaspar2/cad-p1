#include "file2ppm.c"
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define PROGRAM_NAME "myfile2ppm"

int main(int argc, char *argv[]) {
  if (argc < 5) {
    printf("Not enough arguments! %d\n", argc);
    printf("Usage: %s number_of_rows number_of_columns "
           "number_of_simulation_steps folder_with_matrices\n",
           PROGRAM_NAME);
    return 0;
  }

  int nrows = atoi(argv[1]);
  int ncolumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  char *folderName = argv[4];

  struct dirent *de; // Pointer for directory entry
  printf("folderName:%s\n", folderName);
  // opendir() returns a pointer of DIR type.
  DIR *dr = opendir(folderName);

  if (dr == NULL) // opendir returns NULL if couldn't open directory
  {
    printf("Could not open current directory");
    return 0;
  }

  int **load_grid = malloc(nrows * sizeof(int *));
  for (int i = 0; i < nrows; i++) {
    load_grid[i] = malloc(ncolumns * sizeof(int));
  }

  char matrixPath[128];
  // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
  // for readdir()
  // list dir
  int currentStep = 0;
  while ((de = readdir(dr)) != NULL) {
    printf("%s\n", de->d_name);
    if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..") ||
        !strcmp(de->d_name, ".DS_Store")) {
      continue; /* skip self and parent */
    }
    sprintf(matrixPath, "%s/%s", folderName, de->d_name);
    printf("matrixPath:%s!\n", matrixPath); // TODO DEBUG
    fileToGrid(matrixPath, load_grid, nrows, ncolumns, WITH_BORDER);
    printf("fileToGrid ok!"); // TODO DEBUG
    gridToImageFile(folderName, load_grid, currentStep, nrows, ncolumns,
                    WITH_BORDER);
    // ERROR TRAP 6
    currentStep++;
  }

  closedir(dr);
  free(load_grid);

  return 0;
}
